#include "Textures.hh"

#include "Map.hh"
#include "Monster.hh"
#include "Tools.hh"
#include <Utils/Constants.hh>

#include <SFML/System/Vector2.hpp>

#include <iostream>

namespace TowerDefense
{
namespace Textures
{
std::shared_ptr<sf::Texture> LoadTexture(const std::string& filePath)
{
	std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>();
	if (!texture->loadFromFile(filePath))
	{
		std::cerr << "ERROR: Can't load file: " << filePath << std::endl;
		std::exit(1);
	}
	return texture;
}

std::shared_ptr<sf::Texture> GetFloorTexture(const Floor& floor)
{
	switch (floor.GetFloorType())
	{
	case FloorType::START:
		return LoadTexture(Constants::Sprites::START_SPRITE);
	case FloorType::GRASS:
		return LoadTexture(Constants::Sprites::GRASS_SPRITE);
	case FloorType::WATER:
		return LoadTexture(Constants::Sprites::WATER_SPRITE);
	case FloorType::PATH:
		return LoadTexture(Constants::Sprites::PATH_SPRITE);
	case FloorType::FINISH:
		return LoadTexture(Constants::Sprites::FINISH_SPRITE);
	default:
		return std::make_shared<sf::Texture>();
	}
}

TexturePositionsCollection init_textures(const Map& map)
{
	const auto& floorVector = map.GetMap();
	TexturePositionsCollection textureCollection;
	const int width = map.GetWidth();

	for (int i = 0; i < floorVector.size(); ++i)
	{
		const Floor& floor = floorVector[i];
		const auto floorTexture = GetFloorTexture(floor);
		const auto floorCoordinates = Tools::GetPointFromIndex(i, width);

		if (floor.GetFloorType() == FloorType::START ||
		    floor.GetFloorType() == FloorType::FINISH)
		{
			const auto& pathTexture = GetFloorTexture(' ');
			textureCollection.push_back({ pathTexture, floorCoordinates });
		}
		textureCollection.push_back({ floorTexture, floorCoordinates });
	}
	return textureCollection;
}

void draw_map(sf::RenderWindow& w, const TexturePositionsCollection& texturesPositions)
{
	for (const auto& pair : texturesPositions)
	{
		sf::Sprite& tmp = sf::Sprite(*pair.first);
		tmp.setPosition(pair.second.GetAbsoluteX(), pair.second.GetAbsoluteY());
		w.draw(tmp);
	}
}

//Replace monster by std::vector<Monster>
void draw_monster(sf::RenderWindow& w, const Monster& monster)
{
	const auto& texture = monster.GetTexture();
	sf::Sprite& sprite = sf::Sprite(*texture);
	sprite.setPosition(
		monster.GetPosition().GetAbsoluteX(),
		monster.GetPosition().GetAbsoluteY());
	w.draw(sprite);
}

void draw_tower(sf::RenderWindow& w, const Tower::ITower& tower)
{
	const auto& texture = tower.GetTexture();
	sf::Sprite& sprite = sf::Sprite(*texture);
	sprite.setPosition(
		tower.GetPosition().GetAbsoluteX(),
		tower.GetPosition().GetAbsoluteY());
	w.draw(sprite);
}
} // namespace Textures
} // namespace TowerDefense