#pragma once

#include <SFML/Graphics.hpp>

#include <IEngineComponent.hh>
#include <Utils/Position.hh>

#include <memory>

namespace TowerDefense
{

class Monster : public IEngineComponent
{
public:
   /// Constructor.
   /// \param[in] life Life of the monster.
   /// \param[in] speed Speed value of the monster.
   /// \param[in] power Strength of the monster.
   /// \param[in] texture Texture representation of the monster.
   /// \param[in] position Start position of the monster.
   /// \param[in] path Path that the monster should follow.
   Monster(int life, float speed, int price, Position position, std::shared_ptr<sf::Texture> texture);

   /// Default destructor.
   ~Monster() = default;

   /// Get the monster's life.
   /// \return The monster's life.
   int GetLife() const;

   /// Get the monster's speed.
   /// \return The monster's speed.
   float GetSpeed() const;

   /// Get the monster's price.
   /// \return The monster's price.
   int GetPrice() const;

   /// Get the monster's texture.
   /// \return A pointer to the monster's texture.
   std::shared_ptr<sf::Texture> GetTexture() const;

   /// Get the monster's position.
   /// \return A pointer to the monster's position.
   Position GetPosition() const;

   /// Set the monster's life.
   /// \param[in] The new life.
   void SetLife(int life);

   // void setSpeed(int speed);
public:
   // See if we can put the path as an attribute, or do something else than passing the
   // vector for each loop
   /// Operates the monster's movement.
   // void move(const std::vector<Position>& path);

   bool IsArrived();

   /// \see IEngineComponent::Update
   void Update(Engine& e) override;

   /// \see IEngineComponent::Render
   void Render(sf::RenderWindow& window) override;

private:
   /// Life of the entity.
   int life_;

   /// Integer that controls the speed of the entity.
   float speed_;
   int price_;
   int index_ = 0;
   std::shared_ptr<sf::Texture> texture_;
   Position position_;
   /// Indicates if the monster has reached the finish point.
   bool arrived_;
   /// TODO Handle this in another way.
   sf::Clock clock_;
};
}
