#pragma once

#include <Monster.hh>

namespace TowerDefense
{
class Penguin: public Monster
{
public:
   /// see TowerDefense::Monster().
   Penguin(Position position);
};

} // namespace TowerDefense