#pragma once

#include <SFML/Graphics.hpp>

namespace TowerDefense
{

class Engine;

class IEngineComponent
{
public:

   virtual void Update(Engine& e) = 0;

   virtual void Render(sf::RenderWindow& window) = 0;
};

} // namespace TowerDefense
