#include <CustomComponent.hh>

namespace TowerDefense
{
void CustomComponent::Update(Engine& e)
{
   if (updateF_)
      updateF_(e);
}

void CustomComponent::Render(sf::RenderWindow& w)
{
   if (renderF_)
      renderF_(w);
}

void CustomComponent::AddUpdateFunction(std::function<void(Engine&)> f)
{
   updateF_ = f;
}

void CustomComponent::AddRenderFunction(std::function<void(sf::RenderWindow&)> f)
{
   renderF_ = f;
}

} // namespace TowerDefense