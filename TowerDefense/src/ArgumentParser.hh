#pragma once

#include <string>
#include <map>

namespace TowerDefense
{
class ArgumentParser
{
public:
   /// Constructor
   /// \param[in] argc Arguments count 
   /// \param[in] argv Arguments array
   ArgumentParser(int argc, char *argv[]);

   /// Destructor
   ~ArgumentParser() = default;

   const std::string& GetMapFilePath() const;

   const std::string& GetArg(const std::string& arg) const;

private:
   std::map<std::string, std::string> argContainer_;
};
}