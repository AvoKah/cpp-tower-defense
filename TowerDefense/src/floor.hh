#pragma once

#include <ostream>

namespace TowerDefense
{
/// List of the possible floor types.
enum class FloorType
{
    GRASS,
    PATH,
    START,
    FINISH,
    WATER,
    NONE
};

class Floor
{
    public:
		/// Floor ctor.
		/// \param[in] c Char that represents the floor type.
        Floor(const char c);

		/// Default ctor.
		Floor() = delete;

		/// Default destructor.
        ~Floor() = default;

		/// Getter on the enum FloorType.
        FloorType GetFloorType() const;

    private:
		/// Floor type.
        FloorType type_;

	friend std::ostream& operator<<(std::ostream& ostr, Floor floor);
};

} // namespace TowerDefense
