#include "Tools.hh"
#include <Utils/Constants.hh>

namespace TowerDefense
{
namespace Tools
{
	Position GetPointFromIndex(const int index, const int width)
	{
		const int x = index % width;
		const int y = index / width;
		return Position(x, y);
	}

	int GetIndexFromPoint(const Position& point, const int width)
	{
		return (point.GetX() + 1 + (point.GetY() * width)) - 1;
	}
} // namespace Tools
} // namespace TowerDefense