#include <Monster.hh>

#include <Engine.hh>
#include <Textures.hh>

namespace TowerDefense
{
Monster::Monster(int life, float speed, int price, Position position, std::shared_ptr<sf::Texture> texture)
{
   life_ = life;
   speed_ = speed;
   price_ = price;
   position_ = position;
   texture_ = texture;
   arrived_ = false;
}

int Monster::GetLife() const
{
   return life_;
}

float Monster::GetSpeed() const
{
   return speed_;
}

int Monster::GetPrice() const
{
   return price_;
}

std::shared_ptr<sf::Texture> Monster::GetTexture() const
{
   return texture_;
}

Position Monster::GetPosition() const
{
   return position_;
}

void Monster::SetLife(int life)
{
   life_ = life;
}

//void Monster::move(const std::vector<Position>& path)
//{
//   const auto elapsed = clock_.getElapsedTime().asMilliseconds();
//   const auto timelapse = 1000.0 / speed_;
//   if (index_ == path.size() - 1)
//   {
//      arrived_ = true;
//      return;
//   }
//
//   position_.x =
//      (timelapse - elapsed) / timelapse * path.at(index_).x +
//      (elapsed / timelapse) * path.at(index_ + 1).x;
//   position_.y =
//      (timelapse - elapsed) / timelapse * path.at(index_).y +
//      (elapsed / timelapse) * path.at(index_ + 1).y;
//
//   if (elapsed > timelapse)
//   {
//      position_ = path.at(++index_);
//      clock_.restart();
//   }
//}

bool Monster::IsArrived()
{
   return arrived_;
}

void Monster::Update(Engine& e)
{
   //move(e.GetMap().GetPath());
}

void Monster::Render(sf::RenderWindow& window)
{
   Textures::draw_monster(window, *this);
}

} // namespace TowerDefense