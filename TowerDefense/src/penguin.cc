#include <Penguin.hh>

#include <Textures.hh>
#include <Utils/Constants.hh>

namespace TowerDefense
{
Penguin::Penguin(Position position) :
	Monster(10, 5, 10, position, Textures::LoadTexture(Constants::Sprites::PENGUIN))
{
}
} // namespace TowerDefense