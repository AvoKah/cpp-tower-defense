#include <Wave.hh>

#include <Penguin.hh>

#include <Engine.hh>

#include <iostream>

namespace
{
   const int TIME_BEFORE_FIRST_SPAWN_MS = 2000;
   const int TIME_BETWEEN_SPAWNS_MS = 500;
   const int TIME_BETWEEN_WAVES_MS = 300;
}

namespace TowerDefense
{

int Wave::waveIndex_ = 0;
static sf::Clock waveClock;
static bool firstMonsterSpawned_ = false;

void Wave::StartWave(int monstersPerWave)
{
   waveClock.restart();
   waveTimer_ = 0;
   ++waveIndex_;
   monstersToSpawn_ = monstersPerWave;
   //std::vector<std::shared_ptr<Monster>> monsterCollection;
}

int Wave::GetWaveIndex()
{
   return waveIndex_;
}

void Wave::AddMonster()
{
   if (!firstMonsterSpawned_)
      firstMonsterSpawned_ = true;
   if (monstersToSpawn_ > 0)
   {
      monsters_.emplace_back(
         std::make_shared<Penguin>(Position{ 0, 0 }));
      --monstersToSpawn_;
   }
}

// TODO Remove monster when they arrive
void Wave::RemoveMonster(Monster const * monster)
{
   auto it = std::find_if(begin(monsters_), end(monsters_), [monster](auto p)
   {
      return p.get() == monster;
   });
   //if (it == end(monsters_)) return;
   //monsters_.erase(it);
}


void Wave::Update(Engine& e)
{
   elapsedTime_ = waveClock.getElapsedTime().asMilliseconds();
   waveTimer_ += e.GetFrameTime();
   // std::cout << elapsedTime_ << "vs. " << waveTimer_ << std::endl;
   if (!firstMonsterSpawned_ && elapsedTime_ > TIME_BEFORE_FIRST_SPAWN_MS)
   {
      // AddMonster();
      waveClock.restart();
      elapsedTime_ = 0;
      waveTimer_ = 0;
   }
   else if (!firstMonsterSpawned_)
   {
      //std::cout << "Wave Incoming!" << std::endl;
   }
   if (firstMonsterSpawned_ && elapsedTime_ > TIME_BETWEEN_SPAWNS_MS)
   {
      // AddMonster();
      elapsedTime_ = 0;
      waveClock.restart();
      waveTimer_ = 0;
   }

   for (auto&& m : monsters_)
   {
      m->Update(e);
   }

   auto allArrived = true;
   /*for (auto&& m : monsters_)
   {
      allArrived &= m->IsArrived();
   }*/

   if (allArrived && !monsters_.empty())
   {
      monsters_.clear();
      timer_ = 0;
   }
   else if (firstMonsterSpawned_ && monsters_.empty() && monstersToSpawn_ == 0)
   {
      timer_ += e.GetFrameTime();
      std::cout << timer_ << std::endl;
      if (timer_ > TIME_BETWEEN_WAVES_MS)
      {
         StartWave(5);
         timer_ = 0;
      }
   }
}

void Wave::Render(sf::RenderWindow& w)
{
   for (auto&& m : monsters_)
   {
      m->Render(w);
   }
}

} // namespace TowerDefense