#pragma once

#include <string>

namespace TowerDefense
{
namespace Constants
{
/// Size of a single tile.
const static int TILE_SIZE = 64;
namespace Sprites
{
const std::string SPRITE_FOLDER("sprites/");
const std::string SPRITE_SIZE_FOLDER(
    SPRITE_FOLDER + std::to_string(TILE_SIZE) + "/");

const std::string PENGUIN(SPRITE_SIZE_FOLDER + "penguin.png");

/// Floor sprites
const std::string PATH_SPRITE(SPRITE_SIZE_FOLDER + "path.png");
const std::string START_SPRITE(SPRITE_SIZE_FOLDER + "start.png");
const std::string GRASS_SPRITE(SPRITE_SIZE_FOLDER + "grass.png");
const std::string WATER_SPRITE(SPRITE_SIZE_FOLDER + "water.png");
const std::string FINISH_SPRITE(SPRITE_SIZE_FOLDER + "finish.png");

/// Tower sprites
const std::string TOWER_FOLDER(SPRITE_SIZE_FOLDER + "towers/");
const std::string CANNON_SPRITE(TOWER_FOLDER + "Cannon.png");
}
} // namespace Constants
} // namespace TowerDefense