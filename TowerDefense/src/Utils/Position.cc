#include <Utils/Position.hh>
#include <Utils/Constants.hh>
#include <Utils/Convert.hh>
#include <Tools.hh>

#include <iostream>

namespace
{
const int MAX_WIDTH(100);
}

namespace TowerDefense
{
Position::Position(const int x, const int y, bool absolute)
{
    if (absolute)
    {
       SetAbsolutePosition({ x, y });
    }
    else
    {
       SetPosition({ x, y });
    }
}

Position::Position(const sf::Vector2f& pos, bool absolute)
{
   if (absolute)
   {
      SetAbsolutePosition(Convert(pos));
   }
   else
   {
      SetPosition(Convert(pos));
   }
}

int Position::GetX() const
{
    return gamePosition_.x;
}

int Position::GetY() const
{
    return gamePosition_.y;
}

int Position::GetAbsoluteX() const
{
    return absolutePosition_.x;
}

int Position::GetAbsoluteY() const
{
    return absolutePosition_.y;
}

void Position::SetPosition(const sf::Vector2i& pos)
{
   gamePosition_ = pos;
   absolutePosition_ = pos * Constants::TILE_SIZE;
}

void Position::SetAbsolutePosition(const sf::Vector2i& pos)
{
   absolutePosition_ = pos;
   gamePosition_ = pos / Constants::TILE_SIZE;
}

std::ostream& operator<<(std::ostream& os, const Position& p)
{
    os << "Position: (" << p.GetX() << ", " << p.GetY() <<
       ") - Absolute: (" << p.GetAbsoluteX() << ", " << p.GetAbsoluteY() << ")";
    return os;
}

bool operator==(const Position& p1, const Position& p2)
{
   bool res = true;
   res &= p1.GetX() == p2.GetX();
   res &= p1.GetY() == p2.GetY();
   
   return res;
}

bool operator<(const Position& p1, const Position& p2)
{
   using namespace Tools;
   
   return GetIndexFromPoint(p1, MAX_WIDTH) < GetIndexFromPoint(p2, MAX_WIDTH);
}
} // namespace TowerDefense
