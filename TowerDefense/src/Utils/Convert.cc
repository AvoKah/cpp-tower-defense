#include <Utils/Convert.hh>

namespace TowerDefense
{
// Vector conversions.
sf::Vector2i Convert(const sf::Vector2f& v)
{
   sf::Vector2i newV(v.x, v.y);
   return newV;
}

sf::Vector2f Convert(const sf::Vector2i& v)
{
   sf::Vector2f newV(v.x, v.y);
   return newV;
}
} // namespace TowerDefense