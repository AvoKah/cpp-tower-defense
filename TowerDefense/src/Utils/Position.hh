#pragma once

#include <SFML/System/Vector2.hpp>

#include <iostream>

namespace TowerDefense
{
class Position final
{
public:
	/// Default constructor.
	Position() = default;

	/// Constructor.
	/// \param[in] x Absciss value in game coordinates.
	/// \param[in] y Ordinate value in game coordinates.
	/// \param[in] absolute Flag that indicates whether the values are absolute or not.
	/// 		   Default is false.
	Position(const int x, const int y, bool absolute = false);

	/// Constructor.
	/// \param[in] pos Position.
	/// \param[in] absolute Flag that indicates whether the values are absolute or not.
	/// 		   Default is false.
	Position::Position(const sf::Vector2f& pos, bool absolute = false);

	/// Default destructor
	~Position() = default;

	/// Getters on coordinates
	/// @{
	int GetX() const;
	int GetY() const;
	int GetAbsoluteX() const;
	int GetAbsoluteY() const;
	/// @}

	/// Setters on coordinates
	/// @{
	void SetX(const int x);
	void SetY(const int y);
	void SetPosition(const sf::Vector2i& pos);
	
	void SetAbsoluteX(const int x);
	void SetAbsoluteY(const int y);
	void SetAbsolutePosition(const sf::Vector2i& pos);
	/// @}

private:
	/// Position in game coordinates
	sf::Vector2i gamePosition_;

	/// Position in world coordinates (x * TILE_SIZE = gamePosition.x).
	sf::Vector2i absolutePosition_;
};

std::ostream& operator<<(std::ostream& os, const Position& p);

bool operator==(const Position& p1, const Position& p2);

/// For std::map in TowerManager.hh
bool operator<(const Position& p1, const Position& p2);
} // namespace TowerDefense
