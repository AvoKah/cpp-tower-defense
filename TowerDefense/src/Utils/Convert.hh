#pragma once

#include <SFML/System/Vector2.hpp>

namespace TowerDefense
{
   sf::Vector2i Convert(const sf::Vector2f& v);
   sf::Vector2f Convert(const sf::Vector2i& v);
} // namespace TowerDefense