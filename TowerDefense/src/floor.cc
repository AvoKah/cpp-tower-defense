#include <Floor.hh>

#include <iostream>

#define PRETTY_FLOOR

namespace TowerDefense
{

Floor::Floor(const char c)
{
   switch (c)
   {
      case 'B':
         type_ = FloorType::START;
         break;
      case 'g':
         type_ = FloorType::GRASS;
         break;
      case 'w':
         type_ = FloorType::WATER;
         break;
      case ' ':
         type_ = FloorType::PATH;
         break;
      case 'E':
         type_ = FloorType::FINISH;
         break;
      default:
         type_ = FloorType::NONE;
         break;
   }
}

FloorType Floor::GetFloorType() const
{
   return type_;
}

std::ostream& operator<<(std::ostream & ostr, Floor floor)
{
	switch (floor.GetFloorType())
	{
#ifdef PRETTY_FLOOR
	case FloorType::START:
		return ostr << "\033[91mB\033[0m";
	case FloorType::GRASS:
		return ostr << "\033[92mG\033[0m";
	case FloorType::WATER:
		return ostr << "\033[94mW\033[0m";
	case FloorType::PATH:
		return ostr << ' ';
	case FloorType::FINISH:
		return ostr << "\033[91mE\033[0m";
#else
	case FloorType::START:
		return ostr << "B";
	case FloorType::GRASS:
		return ostr << "g";
	case FloorType::WATER:
		return ostr << "w";
	case FloorType::PATH:
		return ostr << ' ';
	case FloorType::FINISH:
		return ostr << "E";
#endif // !PRETTY_FLOOR
	default:
		break;
	}
   return ostr << "ERROR: Unknown floor" << std::endl;
}

} // namespace TowerDefense