#include <GameLauncher.hh>
#include <Utils/Constants.hh>
#include <Textures.hh>
#include <CustomComponent.hh>
#include <gameplay/ClickManager.hh>

// To remove
#include <SFML/Graphics/RectangleShape.hpp>
// !To remove

#include <SFML/Window/Window.hpp>
#include <SFML/Window.hpp>

#define TESTING
//#define TESTWINDOW

namespace
{
using namespace TowerDefense;

void TestNewComponent(const std::shared_ptr<Engine>& engine)
{
   auto component = std::make_shared<CustomComponent>();

   engine->AddComponent(component);
}
} /// anonymous namespace

namespace TowerDefense
{
#ifdef TESTWINDOW
void GameLauncher::Build(const ArgumentParser& args)
{
   const auto& videoMode = sf::VideoMode(800, 600);
   auto window = std::make_unique<sf::RenderWindow>(videoMode, "Tower Defense Lite");
   const auto& engine = std::make_shared<Engine>(std::move(window), nullptr);
   const auto& gameplay = std::make_shared<CustomComponent>();
   gameplay->AddRenderFunction([](sf::RenderWindow& window)
      {
         window.clear();
         sf::RectangleShape rectangle;
         rectangle.setSize(sf::Vector2f(100, 50));
         rectangle.setOutlineColor(sf::Color::Red);
         rectangle.setOutlineThickness(5);
         rectangle.setPosition(10, 20);
         window.draw(rectangle);
      }
   );

   gameplay->AddUpdateFunction([&](Engine& e)
      {
         sf::Event ev;
         e.GetWindow().pollEvent(ev);
         if (ev.type == sf::Event::Closed)
         {
            e.Quit();
         }
      }
   );
   engine->AddComponent(gameplay);
   engine->Run();
}
#else
void GameLauncher::Build(const ArgumentParser& args)
{
   const auto& gameEngine = CreateGameEngine(args.GetMapFilePath());

   const auto& texturesAndPosition = Textures::init_textures(gameEngine->GetMap());
   const auto& gameplay = std::make_shared<CustomComponent>();
   gameplay->AddRenderFunction([&texturesAndPosition = texturesAndPosition](sf::RenderWindow& window)
      {
         window.clear();
         Textures::draw_map(window, texturesAndPosition);
      }
   );

   ClickManager clickManager(gameEngine);
   // auto inputEvent = std::make_shared<CustomComponent>();
   gameplay->AddUpdateFunction([&](Engine& e)
      {
         sf::Event ev;
         e.GetWindow().pollEvent(ev);
         if (ev.type == sf::Event::Closed)
         {
            e.Quit();
         }
         if (ev.type == sf::Event::MouseButtonPressed)
         {
            const auto& mousePosition = sf::Mouse::getPosition(gameEngine->GetWindow());
            clickManager.Click(Position(mousePosition.x, mousePosition.y, true));
         }
      }
   );

   // gameEngine.AddComponent(inputEvent);
   gameEngine->AddComponent(gameplay);

   /*auto wave = std::make_shared<Wave>();
   wave->StartWave(10);
   gameEngine.AddComponent(wave);*/
#ifdef TESTING
   TestNewComponent(gameEngine);
#endif
   gameEngine->Run();
}
#endif
std::shared_ptr<Engine> GameLauncher::CreateGameEngine(const std::string& mapFilePath)
{
   auto map = std::make_unique<const Map>("maps/" + mapFilePath);
   map->Print();

   const auto& videoMode = sf::VideoMode(
      map->GetWidth() * Constants::TILE_SIZE,
      map->GetHeight() * Constants::TILE_SIZE);
   auto window = std::make_unique<sf::RenderWindow>(
      videoMode, "Tower Defense Lite", sf::Style::Titlebar | sf::Style::Close);

   return std::make_shared<Engine>(std::move(window), std::move(map));
}
}
