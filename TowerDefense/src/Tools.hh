#pragma once

#include <utils/Position.hh>

namespace TowerDefense
{

namespace Tools
{
	/// Gets a point coordinates from a vector.
	/// \param[in] index Index of the desired point in a vector.
	/// \param[in] width Width of the map from which we want the point.
	/// \return The point coordinates.
	Position GetPointFromIndex(const int index, const int width);

	/// Gets a point index in a linear vector from its coordinates.
	/// \param[in] point The Point coordinates.
	/// \param[in] width Width of the map from which we want the point.
	/// \return The point index in a 1D vector.
	int GetIndexFromPoint(const Position& point, const int width);
} // namespace Tools
} // namespace TowerDefense