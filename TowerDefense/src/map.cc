#include <Map.hh>

#include <SFML/System/Vector2.hpp>
#include <Tools.hh>
#include <Floor.hh>
#include <Utils/Constants.hh>

#include <fstream>
#include <ostream>
#include <iostream>
#include <memory>
#include <tuple>

#define PRETTY_FLOOR

namespace TowerDefense
{
	Map::Map(const std::string& filename)
	{
		std::fstream file(filename, std::ios::in);
		char c;
		while ((c = file.get()) != EOF)
		{
			if (c == '\n')
			{
				// If width_ has not been set yet.	
				if (width_ == 0)
					width_ = map_.size();
				++height_;
				continue;
			}
			Floor f(c);
			map_.push_back(f);
			const int size = map_.size();
			switch (f.GetFloorType())
			{
				// width_ = 0 only for the first line. so if width_ = 0, we're on the
				// first line so the x = the size of the vector - 1.
			case FloorType::START:
				start_ = Position((width_ == 0) ? size - 1 : ((size - 1) % width_), height_);
				break;
			case FloorType::FINISH:
				finish_ = Position((width_ == 0) ? size - 1 : ((size - 1) % width_), height_);
				break;
			default:
				break;
			}
		}
		file.close();
	}

	int Map::GetWidth() const
	{
		return width_;
	}

	int Map::GetHeight() const
	{
		return height_;
	}

	Position Map::GetStart() const
	{
		return start_;
	}

	Position Map::GetFinish() const
	{
		return finish_;
	}

	std::vector<Floor> Map::GetMap() const
	{
		return map_;
	}

	//Position Map::GetNextPathTile(const Position& ref, std::vector<std::pair<Floor, bool>>& map) const
	//{
	//	std::vector<Position> adjacentPoints = {};
	//	if (ref.x - 1 >= 0)
	//	{
	//		adjacentPoints.emplace_back(Position(ref.x - 1, ref.y));
	//	}
	//	if (ref.y - 1 >= 0)
	//	{
	//		adjacentPoints.emplace_back(Position(ref.x, ref.y - 1));
	//	}
	//	if (ref.x + 1 < width_)
	//	{
	//		adjacentPoints.emplace_back(Position(ref.x + 1, ref.y));
	//	}
	//	if (ref.y + 1 < height_)
	//	{
	//		adjacentPoints.emplace_back(Position(ref.x, ref.y + 1));
	//	}
	//	for (int i = 0; i < adjacentPoints.size(); ++i)
	//	{
	//		const auto& adjacentPoint = adjacentPoints[i];
	//		const auto index = Tools::GetIndexFromPoint(adjacentPoint, width_);
	//		const Floor adjacentFloor = map[index].first;
	//		bool& visited = map[index].second;
	//		if (adjacentFloor.GetFloorType() == FloorType::FINISH)
	//		{
	//			return Tools::GetPointFromIndex(index, width_);
	//		}
	//		else if (adjacentFloor.GetFloorType() != FloorType::PATH || visited)
	//		{
	//			adjacentPoints.erase(adjacentPoints.begin() + i);
	//			i--;
	//		}
	//		else
	//		{
	//			visited = true;
	//		}
	//	}
	//	if (adjacentPoints.size() == 1)
	//		return adjacentPoints[0];
	//	else
	//	{
	//		std::cerr << "[WARNING] There has been more than 1 matching adjacent points." << std::endl;
	//		return Position();
	//	}
	//}

	//void Map::FindMonsterPath(const Position start, std::vector<std::pair<Floor, bool>>& map, std::vector<Position>& vect) const
	//{
	//	if (start == finish_)
	//		return;
	//	else
	//	{
	//		const auto& nextPoint = GetNextPathTile(start, map);
	//		vect.emplace_back(nextPoint * 32.0f);
	//		FindMonsterPath(nextPoint, map, vect);
	//	}
	//}

	//std::vector<Position> Map::GetPath() const
	//{
	//	// The boolean is to ensure that each tile is visited once. bool = visited?
	//	std::vector<std::pair<Floor, bool>> customMap;
	//	for (int i = 0; i < map_.size(); ++i)
	//	{
	//		auto floor = map_[i];
	//		customMap.emplace_back(std::make_pair(floor, false));
	//	}
	//	auto start = start_;
	//	std::vector<Position> returnedPath = { start * Constants::TILE_SIZE};
	//	FindMonsterPath(start, customMap, returnedPath);
	//	return returnedPath;
	//}

	Floor Map::GetFloor(const Position& pos) const
	{
	   if (pos.GetX() < 0 ||
		   pos.GetY() < 0 ||
		   pos.GetX() >= width_ ||
		   pos.GetY() >= height_)
	   {
#ifdef PRETTY_FLOOR
		  std::cerr << "\033[91m";
#endif
		  std::cerr << "Error : invalid position : " << pos;
#ifdef PRETTY_FLOOR
		  std::cerr << "\033[0m";
#endif
		  std::cerr << std::endl;
		  std::exit(1);
		  return Floor(0);
	   }
	   return map_[Tools::GetIndexFromPoint(pos, width_)];
	}

	void Map::Print() const
	{
		for (int index = 0; index < map_.size(); ++index)
		{
			std::cout << map_[index];
			if ((index + 1) % width_ == 0)
				std::cout << std::endl;
		}
		std::cout << "H: " << height_ << ", W: " << width_ << std::endl;
	}
} // namespace TowerDefense