#pragma once

#include <IEngineComponent.hh>

#include <Monster.hh>

namespace TowerDefense
{
class Wave : public IEngineComponent
{
public:

   void StartWave(int monstersPerWave);

   int GetWaveIndex();


   void Update(Engine& e) override;

   void Render(sf::RenderWindow& w) override;

private:
   void AddMonster();
   void RemoveMonster(Monster const * monster);

   static int waveIndex_;
   int monstersToSpawn_;

   sf::Int32 elapsedTime_ = 0;
   sf::Int32 timer_;
   sf::Int32 waveTimer_ = 0;

   std::vector<std::shared_ptr<Monster>> monsters_;
   //std::vector<std::shared_ptr<MonsterAI>> ais_;
};

} // namespace TowerDefense

