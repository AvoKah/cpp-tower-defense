#include <GameLauncher.hh>
#include <ArgumentParser.hh>

#define PRETTY_CONSOLE

int main(int argc, char *argv[])
{
   using namespace TowerDefense;
   
#ifdef PRETTY_CONSOLE
   std::cerr << "\033[91m";
#endif
   const ArgumentParser container(argc, argv);
   GameLauncher::Build(container);
#ifdef PRETTY_CONSOLE
   std::cerr << "\033[0m";
#endif

   return 0;
}
