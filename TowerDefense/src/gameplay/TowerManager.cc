#include <gameplay/TowerManager.hh>

#include <Utils/Position.hh>
#include <towers/Cannon.hh>
#include <Engine.hh>

namespace TowerDefense
{
TowerManager::TowerManager(const std::shared_ptr<Engine>& engine) :
   engine_(engine),
   selectedTower_(SelectedTower::CANNON)
{
}

static int i = 0;

void TowerManager::AddTower(const Position& pos)
{
   std::shared_ptr<Tower::BaseTower> newTower;
  
   switch (selectedTower_)
   {
      case SelectedTower::CANNON:
         newTower = std::make_shared<Tower::Cannon>(pos);
         break;
      default:
         break;
   }

   std::cout << ++i << std::endl;
   towers_[pos] = newTower.get();
   engine_->AddComponent(newTower);
}

void TowerManager::RemoveTower(const Position& pos)
{
   std::cout << --i << std::endl;
   engine_->RemoveComponent(towers_.at(pos));
   towers_.erase(pos);
}

bool TowerManager::HasTower(const Position& pos)
{
   return towers_.count(pos) > 0;
}
} // namespace TowerDefense
