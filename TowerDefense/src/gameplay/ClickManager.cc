#include <gameplay/ClickManager.hh>
#include <gameplay/TowerManager.hh>
#include <Utils/Position.hh>
#include <towers/Cannon.hh>
#include <Engine.hh>

namespace TowerDefense
{
ClickManager::ClickManager(const std::shared_ptr<Engine>& engine) :
   engine_(engine),
   selectedTower_(SelectedTower::CANNON),
   towerManager_(std::make_shared<TowerManager>(engine))
{
}

void ClickManager::Click(const Position& pos)
{
   const auto& floorClicked = engine_->GetMap().GetFloor(pos).GetFloorType();
   if (floorClicked == FloorType::GRASS)
   {
      if (towerManager_->HasTower(pos))
      {
         towerManager_->RemoveTower(pos);
      }
      else
      {
         towerManager_->AddTower(pos);
      }
   }
}
} // namespace TowerDefense