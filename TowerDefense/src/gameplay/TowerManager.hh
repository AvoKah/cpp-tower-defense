#pragma once

#include <memory>
#include <map>

namespace TowerDefense
{
/// Forward declarations
class Position;
class Engine;
namespace Tower
{
class ITower;
} // namespace Tower

class TowerManager final
{
   /// Enum defining which tower is selected.
   enum class SelectedTower
   {
      NONE,
      CANNON
   };

public:
   /// Constructor
   /// \param[in] engine A pointer to the game engine.
   TowerManager(const std::shared_ptr<Engine>& engine);

   /// Default destructor
   ~TowerManager() = default;

   /// Add a tower in the desired position.
   /// \param[in] tilePos The clicked tile position.
   void AddTower(const Position& tilePos);

   /// Remove a tower in the desired position.
   /// \param[in] tilePos The clicked tile position.
   void RemoveTower(const Position& tilePos);

   /// Getter to check if the tile has a tower on it.
   /// \param[in] tilePos The clicked tile position.
   /// \return True if theres a tower at this position.
   bool TowerManager::HasTower(const Position& tilePos);

   // Uprade tower
   // Move tower

private:

   /// A pointer to the game engine.
   std::shared_ptr<Engine> engine_;

   /// Enum corresponding to the selected tower.
   SelectedTower selectedTower_;

   /// Container of Tower's pointers.
   std::map<const Position, const Tower::ITower *> towers_;
};
} // namespace TowerDefense

#include <Engine.hxx>