#pragma once

#include <memory>

namespace TowerDefense
{
class Position;
class Engine;
class TowerManager;

class ClickManager final
{
/// Enum defining which tower is selected.
enum class SelectedTower
{
   NONE,
   CANNON
};

public:
   /// Constructor
   /// \param[in] engine A pointer to the game engine.
   ClickManager(const std::shared_ptr<Engine>& engine);

   /// Default destructor
   ~ClickManager() = default;

   /// Triggered when the user clicked.
   void Click(const Position& pos);

private:
   /// A pointer to the game engine.
   std::shared_ptr<Engine> engine_;

   /// Enum corresponding to the selected tower.
   SelectedTower selectedTower_;

   /// A pointer to the tower manager.
   std::shared_ptr<TowerManager> towerManager_;
};
} // namespace TowerDefense