#include "ArgumentParser.hh"

namespace
{
   const std::string& DEFAULT_INPUT_FILE = "simple2.txt";
}

namespace TowerDefense
{
ArgumentParser::ArgumentParser(int argc, char *argv[])
{
   for (int i = 0; i < argc; ++i)
   {
      const std::string& arg = argv[i];
      if (arg[0] == '-')
      {
         if (arg == "--map" || arg == "-m")
         {
            if (i == argc)
            {
               /// TODO handle this
               return;
            }
            argContainer_["map"] = argv[++i];
         }
         else
         {
            argContainer_[arg.substr(2)] = "";
         }
      }
   }
}

const std::string& ArgumentParser::GetMapFilePath() const
{
   if (argContainer_.find("map") == argContainer_.end())
      return DEFAULT_INPUT_FILE;
   return argContainer_.at("map");
}

const std::string& ArgumentParser::GetArg(const std::string & arg) const
{
   return argContainer_.at(arg);
}

}