#include <towers/Cannon.hh>

#include <Textures.hh>
#include <Utils/Constants.hh>

namespace TowerDefense
{
namespace Tower
{
Cannon::Cannon(Position position) :
	BaseTower(position, Textures::LoadTexture(Constants::Sprites::CANNON_SPRITE))
{
}
} // namespace Tower
} // namespace TowerDefense