#pragma once

#include <IEngineComponent.hh>
#include <Utils/Position.hh>

#include <memory>

namespace TowerDefense
{

namespace Tower
{
class ITower : public IEngineComponent
{
public:

   /// Default copy constructor
   ITower(const ITower&) = default;

   /// Disabled move constructor
   ITower(ITower&&) = delete;

   /// Disabled copy assignment operator
   ITower& operator=(const ITower&) = delete;

   /// Disabled move assignment operator
   ITower& operator=(ITower&&) = delete;

   /// Default destructor
   virtual ~ITower() = default;

   /// \see IEngineComponent::Update
   virtual void Update(Engine& e) = 0;

   /// \see IEngineComponent::Render
   virtual void Render(sf::RenderWindow& window) = 0;

   /// Getter on the texture of the entity.
   /// \return A pointer to the texture.
   virtual std::shared_ptr<sf::Texture> GetTexture() const = 0;

   /// Get the tower's position.
   /// \return The tower's position.
   virtual Position GetPosition() const = 0;

   /// Called to attack a monster.
   // virtual void Attack() = 0;

protected:
   /// Default constructor
   ITower() = default;
};
} // namespace Tower
} // namespace TowerDefense