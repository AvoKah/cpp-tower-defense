#pragma once

#include <towers/ITower.hh>

namespace TowerDefense
{
namespace Tower
{
class BaseTower : public ITower
{
public:
   /// Constructor
   BaseTower(const Position& position, std::shared_ptr<sf::Texture> texture);

   /// Default destructor
   ~BaseTower() override = default;

   /// \see IEngineComponent::Update
   void Update(Engine& e) override;

   /// \see IEngineComponent::Render
   void Render(sf::RenderWindow& window) override;

   /// \see IEngineComponent::GetTexture
   std::shared_ptr<sf::Texture> GetTexture() const override;

   /// \see IEngineComponent::GetPosition
   Position GetPosition() const override;
   
private:
	/// Sprite position
	Position position_;

	/// Sprite texture
	std::shared_ptr<sf::Texture> texture_;
};
} // namespace Tower
} // namespace TowerDefense