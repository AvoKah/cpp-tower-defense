#include <towers/BaseTower.hh>
#include <Textures.hh>
#include <Engine.hh>

#include <Utils/Constants.hh>

namespace TowerDefense
{
namespace Tower
{
BaseTower::BaseTower(const Position& position, std::shared_ptr<sf::Texture> texture) :
   texture_(texture)
{
   position_.SetAbsolutePosition({position.GetX() * Constants::TILE_SIZE,
                                  position.GetY() * Constants::TILE_SIZE});
}

void BaseTower::Update(Engine&)
{
}

void BaseTower::Render(sf::RenderWindow& window)
{
   Textures::draw_tower(window, *this);
}

std::shared_ptr<sf::Texture> BaseTower::GetTexture() const
{
   return texture_;
}

Position BaseTower::GetPosition() const
{
   return position_;
}
} // namespace Tower
} // namespace TowerDefense