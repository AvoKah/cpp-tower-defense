#pragma once

#include <towers/BaseTower.hh>

namespace TowerDefense
{
namespace Tower
{
class Cannon : public BaseTower
{
public:
   /// Constructor
   Cannon(Position position);
};
} // namespace Tower
} // namespace TowerDefense