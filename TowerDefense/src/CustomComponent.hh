#pragma once

#include <IEngineComponent.hh>

#include <functional>

namespace TowerDefense
{
class CustomComponent : public IEngineComponent
{
public:

   void Update(Engine& e) override;

   void Render(sf::RenderWindow& w) override;

   void AddUpdateFunction(std::function<void(Engine&)>);

   void AddRenderFunction(std::function<void(sf::RenderWindow &)>);

private:

   std::function<void(Engine&)> updateF_;

   std::function<void(sf::RenderWindow&)> renderF_;

};
} // namespace TowerDefense
