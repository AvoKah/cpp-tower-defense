#pragma once

#include <Utils/Position.hh>
#include <towers/ITower.hh>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

#include <vector>
#include <memory>

namespace TowerDefense
{
/// Forward declarations.
class Map;
class Floor;
class Monster;

namespace Textures
{
/// A container in which each element is a pair containing a sprite (sf::Texture) and its position
/// in the map (sf::Vector2f).
using TexturePositionsCollection = std::vector<std::pair<std::shared_ptr<sf::Texture>, Position>>;

/// Loads a texture from the disk.
/// \param[in] filePath The filePath to the sprite.
/// \return A sf::Texture that contains the desired sprite.
std::shared_ptr<sf::Texture> LoadTexture(const std::string& filePath);

/// Gets the sprite corresponding to a Floor type
/// \param[in] f The Floor.
/// \return a pointer to the corresponding sprite
std::shared_ptr<sf::Texture> GetFloorTexture(const Floor& f);

/// Initializes Textures and their relative position in the sf::RenderWindow
/// based on the map informations (Floor).
/// \param[in] map The map that will be displayed.
/// \return a vector in which each element is composed of a Texture and its position vector.
TexturePositionsCollection init_textures(const Map& map);

/// Draws a map in the window based on the Textures and their relative position.
/// \param[in] w The main window.
/// \param[in] texturesPositions The container of Textures and their positions.
void draw_map(sf::RenderWindow& w, const TexturePositionsCollection& texturesPositions);

/// Draws a Monster in the window.
/// \param[in] w The main window.
/// \param[in] monster The monster to be drawn.
void draw_monster(sf::RenderWindow& w, const Monster& monster);

/// Draws a Tower in the window.
/// \param[in] w The main window.
/// \param[in] tower The tower to be drawn.
void draw_tower(sf::RenderWindow& w, const Tower::ITower& tower);
} // namespace Textures
} // namespace TowerDefense