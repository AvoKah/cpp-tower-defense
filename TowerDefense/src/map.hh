#pragma once

#include <Floor.hh>
#include <Utils/Position.hh>

#include <vector>

namespace TowerDefense
{

class Map
{
public:
   /// Constructor.
   /// \param[in] filename Map file.	
   Map(const std::string& filename);

   /// Default destructor.
   ~Map() = default;

   /// Copy constructor.
    Map(const Map&) = delete;

   ///// Disabled move constructor.
   Map(Map&&) = default;

   /// Copy assignment operator.
    Map& operator=(const Map&) = delete;

   /// Disabled move assignment operator.
   Map& operator=(Map&&) = default;

public:
   int GetWidth() const;
   int GetHeight() const;
   Position GetStart() const;
   Position GetFinish() const;
   std::vector<Floor> GetMap() const;
   // std::vector<Position> GetPath() const;

   Floor GetFloor(const Position& pos) const;

   /// Display the map in the console.
   void Print() const;

private:
   /// Width of the map
   int width_ = 0;

   /// Height of the map
   int height_ = 0;

   /// Entry point of the entities
   Position start_;

   /// Finishing point of the entities
   Position finish_;

   /// Collection of floors
   std::vector<Floor> map_;

   // void FindMonsterPath(const Position start, std::vector<std::pair<Floor, bool>>& map, std::vector<Position>& vect) const;
   // Position Map::GetNextPathTile(const Position& ref, std::vector<std::pair<Floor, bool>>& map) const;

};
} // namespace TowerDefense
