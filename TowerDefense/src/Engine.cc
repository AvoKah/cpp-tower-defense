#include <Engine.hh>

#include <IEngineComponent.hh>

namespace TowerDefense
{
Engine::Engine(std::unique_ptr<sf::RenderWindow> w, std::unique_ptr<const Map> m):
   window_(std::move(w)),
   map_(std::move(m)),
   frameTimeMs_(0),
   quit_(false)
{
   clock_.restart();
}

void Engine::Run()
{
   while (!quit_)
   {
      Update();
      Render();
   }
}

void Engine::Update()
{
   frameTimeMs_ = clock_.getElapsedTime().asMilliseconds();
   clock_.restart();
   auto temp = components_;
   for (auto&& c : temp)
   {
      c->Update(*this);
   }
}

void Engine::Render()
{
   for (auto&& c : components_)
   {
      c->Render(*window_);
   }
   window_->display();
}

void Engine::Quit()
{
   quit_ = true;
}

sf::RenderWindow& Engine::GetWindow() const
{
   return *window_;
}

const Map& Engine::GetMap() const
{
   return *map_;
}

sf::Int32 Engine::GetFrameTime() const
{
   return frameTimeMs_;
}

void Engine::AddComponent(const std::shared_ptr<IEngineComponent>& component)
{
   components_.emplace_back(component);
}

void Engine::RemoveComponent(const IEngineComponent* component)
{
   auto it = std::find_if(
      begin(components_),
      end(components_),
      [component](auto p)
      {
         return p.get() == component;
      });
   if (it != end(components_))
   {
      components_.erase(it);
   }
}
} // namespace TowerDefense