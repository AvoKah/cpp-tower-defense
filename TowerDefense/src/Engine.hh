#pragma once

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>

#include <Map.hh>

#include <vector>

#include <memory>

namespace TowerDefense
{

class IEngineComponent;

class Engine final
{
public:
   Engine(std::unique_ptr<sf::RenderWindow> w, std::unique_ptr<const Map> m);

   ~Engine() = default;

   /// Copy constructor.
   Engine(const Engine&) = default;

   /// Disabled move constructor.
   Engine(Engine&&) = default;

   /// Copy assignment operator.
   Engine& operator=(const Engine&) = default;

   /// Disabled move assignment operator.
   Engine& operator=(Engine&&) = default;

   void Run();

   void Update();

   void Render();

   void Quit();

   sf::RenderWindow& GetWindow() const;

   const Map& GetMap() const;

   sf::Int32 GetFrameTime() const;

   void AddComponent(const std::shared_ptr<IEngineComponent>& component);

   void RemoveComponent(const IEngineComponent* component);

   template <class T>
   std::vector<T *> FindComponents() const;

private:

   bool quit_;

   sf::Clock clock_;

   sf::Int32 frameTimeMs_;

   std::unique_ptr<const Map> map_;

   std::unique_ptr<sf::RenderWindow> window_;

   std::vector<std::shared_ptr<IEngineComponent>> components_;
};
} // namespace TowerDefense