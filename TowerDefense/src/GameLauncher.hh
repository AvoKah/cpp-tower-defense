#pragma once

#include <Engine.hh>
#include <ArgumentParser.hh>

namespace TowerDefense
{
class GameLauncher final
{
public:
   /// Launches the application.
   static void Build(const ArgumentParser& args);

private:

   /// Create the game engine.
   /// \param[in] mapFilePath File path of the map.
   /// \return The game engine.
   static std::shared_ptr<Engine> GameLauncher::CreateGameEngine(const std::string& mapFilePath);

   /// Add the tower engine that will handle everything linked to the tower in the loopgame.
   /// (i.e attacks, creations, upgrades, ...)
   /// \param[in] gameengine A pointer to the main game engine
   static void GameLauncher::AddTowerEngine(const std::shared_ptr<Engine>& gameEngine);
};
}