#pragma once

#include <vector>
#include <memory>
#include "Engine.hh"

namespace TowerDefense
{
template <class T>
std::vector<T *> Engine::FindComponents() const
{
   std::vector<T *> container;
   std::for_each(
      components_.begin(),
      components_.end(),
      [&container] (const std::shared_ptr<IEngineComponent>& engineComp)
      {
         const auto& typedEngineComp = dynamic_cast<T *>(engineComp.get());
         if (typedEngineComp)
         {
            container.emplace_back(typedEngineComp);
         }
      });
   return container;
}
} // namespace TowerDefense