#include <gtest/gtest.h>

#include <Map.hh>

#include <Penguin.hh>

#include <thread>
#include <chrono>

namespace
{
	const std::string& LITTLE_MAP_PATH = "maps/lol.txt";
	const std::string& MEDIUM_MAP_PATH = "maps/simple.txt";
	const std::string& BIG_MAP_PATH = "maps/hard.txt";
}

using namespace TowerDefense;

namespace Test
{
	/// Test that the monster movements are good.
	/// Test name : Monster.MonsterMovement.
	/// Success : The monster reaches the end of the map.
	TEST(Monster, MonsterMovement)
	{
		// _____
		// |Bgg|
		// | gg|
		// |  E|
		// |ggg|
		// -----
		const Map& map = Map(LITTLE_MAP_PATH);

		const auto& path = map.GetPath();
		const float pengSpeed = 100.0f;
		const auto peng = std::make_shared<Penguin>(map.GetStart());

		ASSERT_EQ(peng->IsArrived(), false);
		EXPECT_EQ(peng->GetPosition(), map.GetStart());

		const auto timer = (1000 / (int)pengSpeed) + 1;

		std::this_thread::sleep_for(std::chrono::milliseconds(timer));
		peng->move(path);

		ASSERT_EQ(peng->IsArrived(), false);
		EXPECT_EQ(peng->GetPosition(), path[1]);

		for (int i = 0; i < path.size() - 1; ++i)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(timer));
			peng->move(path);
		}

		ASSERT_EQ(peng->IsArrived(), true);
		EXPECT_EQ(peng->GetPosition(), path[path.size() - 1]);

		/// \todo Update when slows are implemented.
	}
	
	/// Test that the Monsters life changes depending on what effect they're on.
	/// Test name : Monster.MonsterLife.
	/// Success : Monsters life are correctly updated.
	TEST(Monster, MonsterLife)
	{
		// _____
		// |Bgg|
		// | gg|
		// |  E|
		// |ggg|
		// -----
		const Map& map = Map(LITTLE_MAP_PATH);

		const auto peng = std::make_shared<Penguin>(map.GetStart());

		EXPECT_EQ(peng->GetLife(), 10);
		/// \todo Update when towers are implemented.
	}
} // namespace Test