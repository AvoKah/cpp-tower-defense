#include <gtest/gtest.h>

#include "Map.hh"
#include "Tools.hh"

namespace
{
const std::string& LITTLE_MAP_PATH = "maps/lol.txt";
const std::string& MEDIUM_MAP_PATH = "maps/simple.txt";
const std::string& BIG_MAP_PATH = "maps/hard.txt";
}

using namespace TowerDefense;

namespace Test
{
/// Test that we get the correct Map informations.
/// Test name : Map.LittleMapInformations.
/// Success : The map is correctly constructed.
TEST(Map, LittleMapInformations)
{
	// _____
	// |Bgg|
	// | gg|
	// |  E|
	// |ggg|
	// -----
	const Map& map = Map(LITTLE_MAP_PATH);

	const int width = map.GetWidth();

	EXPECT_EQ(map.GetStart(), sf::Vector2f(0, 0));
	EXPECT_EQ(map.GetFinish(), sf::Vector2f(2, 2));
	EXPECT_EQ(map.GetMap().size(), 3 * 4);
	EXPECT_EQ(width, 3);
	EXPECT_EQ(map.GetHeight(), 4);

	const auto& floor = map.GetFloor(0, 1);
	EXPECT_EQ(floor.GetFloorType(), FloorType::PATH);

	// Bgg gg  Eggg
	const auto& vector = map.GetMap();

	EXPECT_EQ(vector.size(), 3 * 4);
	EXPECT_EQ(vector[9].GetFloorType(), FloorType::GRASS);
	EXPECT_EQ(map.GetFinish(), Tools::GetPointFromIndex(8, width));

	const int index = Tools::GetIndexFromPoint(map.GetFinish(), width);
	EXPECT_EQ(vector[index].GetFloorType(), FloorType::FINISH);

	const auto& path = map.GetPath();
	EXPECT_EQ(path.size(), 5); // Begin  and End count as a path
}

/// Test that we get the correct Map informations.
/// Test name : Map.BigMapInformations.
/// Success : The map is correctly constructed.
TEST(Map, BigMapInformations)
{
	//  _________________________________________
	// |Bgggggggggggggggggggggggggggggggggggggggg|
	// | ggggggggg            ggggggggggggggggggg|
	// | ggggggggg gggggggggg ggggggggggggggggggg|
	// |           gggggggggg gggwwwwgggggggggggg|
	// |ggggggggwwwwggggggggg gggwwwwgggggggggggg|
	// |ggggggggwwwwggggggggg gggwwwwgggggggggggg|
	// |ggggggg               gggwwwwgggggggggggg|
	// |ggggggg wgggggggggggggggggggggggggggggggg|
	// |ggggggg wggggggggggggggggggggggggggg    E|
	// |ggggggg wggggggggggggggggggggggggggg gggg|
	// |ggggggg gggggggggggggggggggggggggggg gggg|
	// |ggggggg                       gggggg gggg|
	// |ggggggggggggggggggggggggggwwg gggggg gggg|
	// |ggggggggggggggggggggggggggwwg gggggg gggg|
	// |ggggggggggggggggggggggggggwwg gggggg gggg|
	// |ggggggggggggggggggggggggggggg gggggg gggg|
	// |ggggggggg                     gggggg gggg|
	// |ggggggggg gggggggggggggggggggggggggg gggg|
	// |ggggggggg gggggggggggggggggggggggggg gggg|
	// |ggggggggg                            gggg|
	// |ggggggggggggggggggggggggggggggggggggggggg|
	// -------------------------------------------
	const Map& map = Map(BIG_MAP_PATH);

	const int width = map.GetWidth();

	EXPECT_EQ(map.GetStart(), sf::Vector2f(0, 0));
	EXPECT_EQ(map.GetFinish(), sf::Vector2f(40, 8));
	EXPECT_EQ(width, 41);
	EXPECT_EQ(map.GetHeight(), 21);

	const auto& floor = map.GetFloor(26, 5);
	EXPECT_EQ(floor.GetFloorType(), FloorType::WATER);

	const auto& vector = map.GetMap();

	EXPECT_EQ(vector.size(), 41 * 21);
	EXPECT_EQ(vector[216].GetFloorType(), FloorType::WATER);
	const auto& supposedFinishPoint = Tools::GetPointFromIndex(368, width);
	EXPECT_EQ(map.GetFinish(), supposedFinishPoint);

	const int index = Tools::GetIndexFromPoint(map.GetFinish(), width);
	EXPECT_EQ(vector[index].GetFloorType(), FloorType::FINISH);
}

/// Test that we get the correct error messages/behaviors.
/// Test name : Map.InvalidValues.
/// Success : Correct behaviors are caught.
TEST(Map, InvalidValues)
{
	// _____
	// |Bgg|
	// | gg|
	// |  E|
	// |ggg|
	// -----
	const Map& map = Map(LITTLE_MAP_PATH);

	EXPECT_EXIT(map.GetFloor(5, 26), ::testing::ExitedWithCode(1), ".*over.*bounds.*");
	EXPECT_EXIT(map.GetFloor(-2, 1), ::testing::ExitedWithCode(1), ".*value.");
}
} // namespace Test