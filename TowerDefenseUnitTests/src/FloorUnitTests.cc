#include <gtest/gtest.h>

#include "Map.hh"

using namespace TowerDefense;

namespace Test
{
/// Test that we get the correct floor types.
/// Test name : Floor.GetFloorTypes
/// Success : The floor types matches the instances created.
TEST(Floor, GetFloorTypes)
{
	EXPECT_EQ(Floor().GetFloorType(), FloorType::NONE);
	EXPECT_EQ(Floor('B').GetFloorType(), FloorType::START);
	EXPECT_EQ(Floor('g').GetFloorType(), FloorType::GRASS);
	EXPECT_EQ(Floor('w').GetFloorType(), FloorType::WATER);
	EXPECT_EQ(Floor(' ').GetFloorType(), FloorType::PATH);
	EXPECT_EQ(Floor('E').GetFloorType(), FloorType::FINISH);
	EXPECT_EQ(Floor('F').GetFloorType(), FloorType::NONE);
	EXPECT_EQ(Floor('\0').GetFloorType(), FloorType::NONE);
}
} // namespace Test