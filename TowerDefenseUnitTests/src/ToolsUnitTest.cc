#include <gtest/gtest.h>

#include "Tools.hh"
#include <SFML/System/Vector2.hpp>

using namespace TowerDefense;

namespace Test
{

/// Test that the conversion of a point from a 2D vector to 1D vector is correct.
/// Test name : Tools.GetIndexFromPoint.
/// Success : The method returns the vector index of the point
///			  matching the given Point coordinates.
TEST(Tools, GetIndexFromPoint)
{
	EXPECT_EQ(Tools::GetIndexFromPoint({ 4, 1 }, 66), 70);
	EXPECT_EQ(Tools::GetIndexFromPoint({ 4, 1 }, 6), 10);
	EXPECT_EQ(Tools::GetIndexFromPoint({ 4, 1 }, 5), 9);
}

/// Test that the conversion of a point from 1D vector to 2D vector is correct.
/// Test name : Tools.GetPointFromIndex.
/// Success : The method returns the coordinates of the point
///			  matching the given index from a vector.
TEST(Tools, GetPointFromIndex)
{
	EXPECT_EQ(Tools::GetPointFromIndex(70, 66), sf::Vector2f({ 4, 1 }));
	EXPECT_EQ(Tools::GetPointFromIndex(10, 6), sf::Vector2f({ 4, 1 }));
	EXPECT_EQ(Tools::GetPointFromIndex(9, 5), sf::Vector2f({ 4, 1 }));
}
} // namespace Test