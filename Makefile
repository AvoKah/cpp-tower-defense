CXX=clang++
CXXFLAGS=-Wextra -Wall -Werror -pedantic -std=c++1y -g
LDFLAGS=-L/usr/local/lib \
-lsfml-window -lGLU -lGL -lsfml-graphics \
         -lsfml-window -lsfml-system
SRC=main.cc maps/map.cc maps/floor.cc display/load.cc display/display.cc
SRC+= monster/monster.cc monster/penguin.cc
SRC:=$(addprefix src/,$(SRC))
DEL= *.o *~ .*.swp *.so *.class *.log *.a
BIN=game

all:$(BIN)

$(BIN): $(SRC)
	$(CXX) $(CXXFLAGS) $(LDFLAGS)  $(SRC) -o $(BIN)

clean:
	rm -rf $(DEL) $(BIN) */$(DEL) */*/$(DEL)
